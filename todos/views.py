from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, ItemForm

def todolist(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list
    }
    return render(request, "todos/list.html", context)

def todoshow(request, id):
    todo_object = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo_object
    }
    return render(request, "todos/detail.html", context)

def todocreate(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_instance = form.save()
            return redirect("todo_list_detail", id=todo_instance.id)
    else:
        form = TodoListForm()

    context = {
        "form" : form,
    }
    return render(request, "todos/create.html", context)

def todoupdate(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm(instance=todo)

    context = {
        "form": form
    }

    return render(request, "todos/edit.html", context)

def tododelete(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def itemcreate(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()
    context = {
        "form" : form
    }

    return render(request, "todos/itemcreate.html", context)

def itemupdate(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm(instance=item)
    context = {
        "form" : form
    }

    return render(request, "todos/itemedit.html", context)
